package com.xunqi.servicebase.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-27 13:02
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuliException extends RuntimeException {

    //状态码
    private Integer code;

    //异常信息
    private String msg;
}
