package com.xunqi.staservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xunqi.commonutils.R;
import com.xunqi.staservice.entity.StatisticsDaily;
import com.xunqi.staservice.feign.UcenterFeign;
import com.xunqi.staservice.mapper.StatisticsDailyMapper;
import com.xunqi.staservice.service.StatisticsDailyService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-13
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    private UcenterFeign ucenterFeign;

    @Override
    public void registerCount(String day) {

        //添加记录之前删除表相同日期的数据
        this.baseMapper.delete(new QueryWrapper<StatisticsDaily>().eq("date_calculated", day));

        R registerR = ucenterFeign.countRegister(day);
        Integer countRegister = (Integer) registerR.getData().get("countRegister");

        //把获取到的数据添加到数据库中
        StatisticsDaily statisticsDaily = new StatisticsDaily();
        statisticsDaily.setRegisterNum(countRegister);  //注册人数
        statisticsDaily.setDateCalculated(day);     //统计日期

        statisticsDaily.setVideoViewNum(RandomUtils.nextInt(100,200));
        statisticsDaily.setLoginNum(RandomUtils.nextInt(100,200));
        statisticsDaily.setCourseNum(RandomUtils.nextInt(100,200));

        this.baseMapper.insert(statisticsDaily);

    }


    /**
     * 图表显示,返回两部分数据，日期json数组，数量json数组
     * @param type
     * @param begin
     * @param end
     * @return
     */
    @Override
    public Map<String, Object> getShowData(String type, String begin, String end) {

        //根据条件查询对应的数据
        QueryWrapper<StatisticsDaily> queryWrapper = new QueryWrapper<>();
        queryWrapper.between("date_calculated",begin,end);
        queryWrapper.select("date_calculated",type);

        List<StatisticsDaily> statisticsDailies = baseMapper.selectList(queryWrapper);

        //因为返回有两部分数据：日期和日期对应数量
        //前端要求传过去数组结构，对应后端Java代码是list集合
        //创建两个list集合，一个日期list，一个数量list
        List<String> date_calculatedList = new ArrayList<>();
        List<Integer> numDataList = new ArrayList<>();

        //遍历查询出来所有数据list集合，进行封装
        for (int i = 0; i < statisticsDailies.size(); i++) {
            StatisticsDaily statisticsDaily = statisticsDailies.get(i);
            //封装日期list集合
            date_calculatedList.add(statisticsDaily.getDateCalculated());
            //封装对应数量
            switch (type) {
                case "register_num":
                    numDataList.add(statisticsDaily.getRegisterNum());
                    break;
                case "login_num":
                    numDataList.add(statisticsDaily.getLoginNum());
                    break;
                case "video_view_num":
                    numDataList.add(statisticsDaily.getVideoViewNum());
                    break;
                case "course_num":
                    numDataList.add(statisticsDaily.getCourseNum());
                    break;
                default:
                    break;
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("date_calculatedList",date_calculatedList);
        map.put("numDataList",numDataList);

        return map;
    }
}
