package com.xunqi.staservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-13 11:36
 **/

@EnableScheduling       //开启定时任务
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.xunqi"})
@MapperScan("com.xunqi.staservice.mapper")
public class StatisticsApplication {

    public static void main(String[] args) {

        SpringApplication.run(StatisticsApplication.class,args);

    }

}
