package com.xunqi.eduservice.mapper;

import com.xunqi.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-07-26
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
