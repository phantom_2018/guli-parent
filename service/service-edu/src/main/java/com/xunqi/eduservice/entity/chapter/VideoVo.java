package com.xunqi.eduservice.entity.chapter;

import lombok.Data;

/**
 * @Description: 视频小节
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-02 10:04
 **/

@Data
public class VideoVo {

    private String id;

    private String title;

    private String videoSourceId;       //视频id
}
