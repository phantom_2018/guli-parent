package com.xunqi.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xunqi.commonutils.JwtUtils;
import com.xunqi.eduservice.entity.EduComment;
import com.xunqi.eduservice.feign.UcenterFeign;
import com.xunqi.eduservice.mapper.EduCommentMapper;
import com.xunqi.eduservice.service.EduCommentService;
import com.xunqi.eduservice.vo.UcenterMemberVo;
import com.xunqi.servicebase.exception.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {

    @Autowired
    private UcenterFeign ucenterFeign;

    /**
     * 添加评论信息
     * @param eduComment
     * @param request
     */
    @Override
    public boolean saveCommont(EduComment eduComment, HttpServletRequest request) {

        //先从token中获取用户信息id
        String jwtToken = JwtUtils.getMemberIdByJwtToken(request);
        if (StringUtils.isEmpty(jwtToken)) {
            throw new GuliException(20001,"请先登录，再来评论！");
        }

        //调用用户服务查询用户信息
        UcenterMemberVo userInfo = ucenterFeign.getByTokenUcenterInfo(jwtToken);
        if (userInfo == null) {
            throw new GuliException(20001,"请先登录，再来评论！");
        }

        //进行赋值
        eduComment.setMemberId(userInfo.getId());
        eduComment.setNickname(userInfo.getNickname());
        eduComment.setAvatar(userInfo.getAvatar());

        //添加到数据库
        int insert = this.baseMapper.insert(eduComment);
        //判断是否新增成功
        if (insert > 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 分页查询评论信息
     * @param commentPage
     * @param courseId
     * @return
     */
    @Override
    public Map<String, Object> getCommentPageInfo(Page<EduComment> commentPage, String courseId) {

        QueryWrapper<EduComment> queryWrapper = new QueryWrapper<>();

        if (!StringUtils.isEmpty(courseId)) {
            queryWrapper.eq("course_id", courseId);
        }
        queryWrapper.orderByDesc("gmt_create");

        IPage<EduComment> commentIPage = this.baseMapper.selectPage(commentPage, queryWrapper);

        List<EduComment> records = commentIPage.getRecords();

        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", commentIPage.getCurrent());
        map.put("pages", commentIPage.getPages());
        map.put("size", commentIPage.getSize());
        map.put("total", commentIPage.getTotal());
        map.put("hasNext", commentPage.hasNext());
        map.put("hasPrevious", commentPage.hasPrevious());

        return map;
    }
}
