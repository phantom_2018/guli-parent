package com.xunqi.eduservice.controller;


import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduVideo;
import com.xunqi.eduservice.feign.VodFeign;
import com.xunqi.eduservice.service.EduVideoService;
import com.xunqi.servicebase.exception.GuliException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
@Api(value = "课程视频管理",tags = "课程视频管理")
@RestController
@RequestMapping("/eduservice/video")
public class EduVideoController {

    @Autowired
    private EduVideoService videoService;

    @Autowired
    private VodFeign vodFeign;

    /**
     * 添加小节信息
     * @param eduVideo
     * @return
     */
    @ApiOperation(value = "添加小节信息",notes = "添加小节信息",httpMethod = "POST")
    @PostMapping(value = "/addVideo")
    public R addVideo(@ApiParam(name = "eduVideo",value = "小节信息对象",required = true)
                      @RequestBody EduVideo eduVideo) {
        videoService.save(eduVideo);
        return R.ok();
    }

    /**
     * 删除小节信息
     * @param id
     * @return
     */
    @ApiOperation(value = "删除小节信息",notes = "删除小节信息",httpMethod = "DELETE")
    @DeleteMapping(value = "/deleteVideo/{id}")
    public R deleteVideo(@ApiParam(name = "id",value = "信息id",required = true)
                         @PathVariable("id") String id) {

        //根据小节id获取视频id
        EduVideo eduVideo = this.videoService.getById(id);
        String videoSourceId = eduVideo.getVideoSourceId();

        //判断小节里面是否有云视频id，有就删
        if (!StringUtils.isEmpty(videoSourceId)) {
            //远程调用删除云端视频资源
            R result = vodFeign.removeAliyVideo(videoSourceId);
            if (result.getCode() == 20001) {
                throw new GuliException(20001,"删除视频失败,熔断器...");
            }
        }

        videoService.removeById(id);
        return R.ok();
    }

    /**
     * 根据id查询小节信息
     */
    @ApiOperation(value = "根据id查询小节信息",notes = "根据id查询小节信息",httpMethod = "GET")
    @GetMapping(value = "/getVideoInfo/{id}")
    public R getVideoInfo(@ApiParam(name = "id",value = "信息id",required = true)
                              @PathVariable("id") String id) {

        EduVideo eduVideo = videoService.getById(id);
        return R.ok().data("video",eduVideo);
    }

    /**
     * 更新小节信息
     * @param eduVideo
     * @return
     */
    @ApiOperation(value = "更新小节信息",notes = "更新小节信息",httpMethod = "PUT")
    @PutMapping(value = "/updateVideo")
    public R updateVideo(@ApiParam(name = "eduVideo",value = "小节信息对象",required = true)
                         @RequestBody EduVideo eduVideo) {

        String id = eduVideo.getId();
        EduVideo eduVideoInfo = this.videoService.getById(id);

        //修改小节信息之前判断云端是否有该小节视频，若有先删除，再添加
        if (!StringUtils.isEmpty(eduVideoInfo.getVideoSourceId())) {
            vodFeign.removeAliyVideo(eduVideoInfo.getVideoSourceId());
        }

        videoService.updateById(eduVideo);
        return R.ok();
    }


    //根据视频id查询小节信息
    @GetMapping(value = "/getVideoSourceIdByInfo")
    public R getVideoSourceIdByInfo(@RequestParam("videoSourceId") String videoSourceId) {

        EduVideo eduVideo = this.videoService.getVideoSourceIdByInfo(videoSourceId);

        return R.ok().data("eduVideo",eduVideo);
    }

}

