package com.xunqi.eduservice.controller;

import com.xunqi.commonutils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-27 21:03
 **/

@Api(value = "用户登录管理",tags = "用户登录管理")
@RestController
@RequestMapping(value = "/eduservice/user")
public class EduLoginController {

    @ApiOperation(value = "用户登录",notes = "用户登录",httpMethod = "POST")
    @PostMapping(value = "/login")
    public R login() {

        return R.ok().data("token","admin");
    }


    @ApiOperation(value = "用户信息",notes = "用户信息",httpMethod = "GET")
    @GetMapping(value = "/info")
    public R info() {

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("roles","[admin]");
        map.put("name","admin");
        map.put("avatar","https://picb.zhimg.com/v2-f0c8a25168c6c46786bf81c5eda5e292_r.jpg?source=1940ef5c");
        return R.ok().data(map);
    }

    @ApiOperation(value = "用户注销",notes = "用户注销",httpMethod = "POST")
    @PostMapping(value = "/logout")
    public R logout() {
        return R.ok();
    }
}
