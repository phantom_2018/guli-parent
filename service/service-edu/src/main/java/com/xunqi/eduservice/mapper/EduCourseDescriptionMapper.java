package com.xunqi.eduservice.mapper;

import com.xunqi.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */

@Repository
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
