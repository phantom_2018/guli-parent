package com.xunqi.eduservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunqi.commonutils.R;
import com.xunqi.commonutils.constant.CourseConstant;
import com.xunqi.eduservice.entity.EduCourse;
import com.xunqi.eduservice.service.EduCourseService;
import com.xunqi.eduservice.vo.CourseInfoVo;
import com.xunqi.eduservice.vo.CoursePublishVo;
import com.xunqi.eduservice.vo.EduCourseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
@Api(value = "课程信息管理",tags = "课程信息管理")
@RestController
@RequestMapping("/eduservice/educourse")
public class EduCourseController {

    @Autowired
    private EduCourseService courseService;

    //课程列表
    @ApiOperation(value = "课程列表",notes = "课程列表",httpMethod = "POST")
    @PostMapping(value = "/{page}/{limit}")
    public R getCourseList(@ApiParam(name = "page", value = "当前页码", required = true)@PathVariable String page,
                           @ApiParam(name = "limit", value = "每页记录数", required = true)@PathVariable String limit,
                           @ApiParam(name = "eduCourseVo",value = "课程查询对象",required = false)
                           @RequestBody(required = false) EduCourseVo eduCourseVo) {

        IPage<EduCourse> eduCourseIPage =  courseService.pageCourseCondition(Long.valueOf(page),Long.valueOf(limit),eduCourseVo);

        //得到总记录数
        long total = eduCourseIPage.getTotal();
        //得到数据list集合
        List<EduCourse> courseList = eduCourseIPage.getRecords();

        return R.ok().data("courseList",courseList).data("total",total);
    }

    /**
     * 添加课程基本信息的方法
     */
    @ApiOperation(value = "添加课程基本信息",notes = "添加课程基本信息",httpMethod = "POST")
    @PostMapping(value = "/addCourseInfo")
    public R addCourseInfo(@ApiParam(name = "courseInfoVo",value = "课程信息对象",required = true)
                           @RequestBody CourseInfoVo courseInfoVo) {

        //返回添加之后的课程ID，为了后面添加大纲使用
        String courseId = courseService.saveCourseInfo(courseInfoVo);

        return R.ok().data("courseId",courseId);
    }

    /**
     * 根据课程id查询课程基本信息
     * @param courseId
     * @return
     */
    @ApiOperation(value = "根据课程id查询课程信息",notes = "根据课程id查询课程信息",httpMethod = "GET")
    @GetMapping(value = "/getCourseInfo/{courseId}")
    public R getCourseInfo(@ApiParam(name = "courseId",value = "课程信息id",required = true)
                           @PathVariable("courseId") String courseId) {

        CourseInfoVo courseInfoVo = courseService.getCourseInfo(courseId);

        return R.ok().data("courseInfo",courseInfoVo);
    }


    /**
     * 修改课程信息
     * @param courseInfoVo
     * @return
     */
    @ApiOperation(value = "修改课程信息",notes = "修改课程信息",httpMethod = "PUT")
    @PutMapping(value = "/updateCourseInfo")
    public R updateCourseInfo(@ApiParam(name = "courseInfoVo",value = "课程信息对象",required = true)
                              @RequestBody CourseInfoVo courseInfoVo) {

        courseService.updateCourseInfo(courseInfoVo);

        return R.ok();
    }


    /**
     * 根据课程id查询课程确认信息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据课程id查询课程确认信息",notes = "根据课程id查询课程确认信息",httpMethod = "DELETE")
    @GetMapping(value = "/getPublishCourseInfo/{id}")
    public R getPublishCourseInfo(@ApiParam(name = "id",value = "课程id",required = true) @PathVariable("id") String id) {

        CoursePublishVo vo = courseService.publishCourseInfo(id);

        return R.ok().data("publishVo",vo);
    }


    /**
     * 课程最终发布
     */
    @ApiOperation(value = "课程最终发布",notes = "课程最终发布",httpMethod = "POST")
    @PostMapping(value = "/publishCourse/{id}")
    public R publishCourse(@ApiParam(name = "id",value = "课程id",required = true) @PathVariable("id") String id) {

        //修改课程状态
        EduCourse eduCourse = new EduCourse();
        //设置课程的发布状态
        eduCourse.setStatus(CourseConstant.NORMAL);
        eduCourse.setId(id);
        courseService.updateById(eduCourse);

        return R.ok();
    }


    /**
     * 删除课程信息
     * @param courseId
     * @return
     */
    @ApiOperation(value = "删除课程信息",notes = "删除课程信息",httpMethod = "DELETE")
    @DeleteMapping(value = "/deleteCourse/{courseId}")
    public R deleteCourse(@ApiParam(name = "courseId",value = "课程id",required = true) @PathVariable("courseId") String courseId) {

        courseService.removeCourse(courseId);

        return R.ok();
    }


    /**
     * 课程购买数量+1，提供给订单服务调用
     * @param courseId
     * @return
     */
    @GetMapping(value = "/addBuyCount")
    public R addBuyCount(@RequestParam("courseId") String courseId) {

        boolean flag = courseService.addBuyCount(courseId);

        if (flag) {
            return R.ok();
        }
        return R.error().message("订单出现异常");
    }


}

