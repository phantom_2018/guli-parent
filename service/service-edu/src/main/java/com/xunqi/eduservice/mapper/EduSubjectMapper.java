package com.xunqi.eduservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xunqi.eduservice.entity.EduSubject;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * 课程科目
 * 
 * @author 夏沫止水
 * @email HeJieLin@gulimall.com
 * @date 2020-07-29 15:22:28
 */
@Repository
@Mapper
public interface EduSubjectMapper extends BaseMapper<EduSubject> {
	
}
