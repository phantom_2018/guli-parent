package com.xunqi.eduservice.mapper;

import com.xunqi.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xunqi.eduservice.vo.CoursePublishVo;
import com.xunqi.eduservice.vo.CourseWebVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */

public interface EduCourseMapper extends BaseMapper<EduCourse> {

    /**
     * 根据课程id查询课程详细信息
     * @param courseId
     * @return
     */
    CoursePublishVo getPublishCourseInfo(@Param("courseId") String courseId);

    /**
     * 根据课程id查询课程详细信息
     * @param courseId
     * @return
     */
    CourseWebVo getBaseCourseInfo(@Param("courseId") String courseId);
}
