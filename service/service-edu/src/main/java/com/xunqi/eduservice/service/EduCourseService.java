package com.xunqi.eduservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xunqi.eduservice.vo.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
public interface EduCourseService extends IService<EduCourse> {

    /**
     * 添加课程基本信息的方法
     * @param vo
     */
    String saveCourseInfo(CourseInfoVo vo);

    /**
     * 根据课程id查询课程基本信息
     * @param courseId
     * @return
     */
    CourseInfoVo getCourseInfo(String courseId);

    /**
     * 修改课程信息
     * @param courseInfoVo
     */
    void updateCourseInfo(CourseInfoVo courseInfoVo);

    /**
     * 根据课程id查询课程详细信息
     * @param id
     * @return
     */
    CoursePublishVo publishCourseInfo(String id);

    /**
     * 多条件分页查询课程信息
     * @param page
     * @param limit
     * @param eduCourseVo
     * @return
     */
    IPage<EduCourse> pageCourseCondition(Long page, Long limit, EduCourseVo eduCourseVo);

    /**
     * 根据课程id删除课程信息
     * @param courseId
     */
    void removeCourse(String courseId);

    /**
     * 根据讲师id查询所讲课程
     * @param teacherId
     * @return
     */
    List<EduCourse> getTeacherIdByInfo(String teacherId);

    Map<String, Object> getFrontCourseList(Page<EduCourse> coursePage, CourseFrontVo courseFrontVo);

    /**
     * 根据课程id查询课程详细信息
     * @param courseId
     * @return
     */
    CourseWebVo getBaseCourseInfo(String courseId);

    /**
     * 购买数量+1
     * @param courseId
     */
    boolean addBuyCount(String courseId);
}
