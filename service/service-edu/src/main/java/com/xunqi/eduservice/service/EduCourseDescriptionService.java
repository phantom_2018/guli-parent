package com.xunqi.eduservice.service;

import com.xunqi.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
