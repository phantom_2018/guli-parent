package com.xunqi.vodtest;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-04 17:29
 **/
public class TestVod {

    /*获取播放地址函数*/
    public static GetPlayInfoResponse getPlayInfo(DefaultAcsClient client) throws Exception {
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        //向request对象里面设置视频id
        request.setVideoId("42e5ec0b7ec540e9a1ed0c2b07ce137e");
        //调用初始化对象里面的方法，传递request，获取数据
        return client.getAcsResponse(request);
    }

    /*获取播放凭证函数*/
    public static GetVideoPlayAuthResponse getVideoPlayAuth(DefaultAcsClient client) throws Exception {
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        request.setVideoId("8cd53441d3b24030b77bf9ce351c7c32");
        return client.getAcsResponse(request);
    }

    public static void main(String[] args) throws Exception {

        // String accessKeyId = "LTAI4GDKuJeGsyXqwLh16gYn";
        // String accessKeySecret = "Yye0jN1m7O81zE0LHVosBz8d8ZWQer";
        // String title = "测试";      //上传之后的文件名称
        // String fileName = "D:/科泰Y2/Y2项目/在线教育--谷粒学院/项目资料/1-阿里云上传测试视频/6 - What If I Want to Move Faster.mp4";   //本地文件的路径和名称
        // UploadVideoRequest request = new UploadVideoRequest(accessKeyId, accessKeySecret, title, fileName);
        // /* 可指定分片上传时每个分片的大小，默认为2M字节 */
        // request.setPartSize(2 * 1024 * 1024L);
        // /* 可指定分片上传时的并发线程数，默认为1，(注：该配置会占用服务器CPU资源，需根据服务器情况指定）*/
        // request.setTaskNum(1);
        //
        // UploadVideoImpl uploader = new UploadVideoImpl();
        // UploadVideoResponse response = uploader.uploadVideo(request);
        // System.out.print("RequestId=" + response.getRequestId() + "\n");  //请求视频点播服务的请求ID
        // if (response.isSuccess()) {
        //     System.out.print("VideoId=" + response.getVideoId() + "\n");
        // } else {
        //     /* 如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因 */
        //     System.out.print("VideoId=" + response.getVideoId() + "\n");
        //     System.out.print("ErrorCode=" + response.getCode() + "\n");
        //     System.out.print("ErrorMessage=" + response.getMessage() + "\n");
        // }
        getPlayAuth();

    }

    /**
     * 根据视频的id获取视频凭证
     */
    private static void getPlayAuth() {
        //获取视频播放凭证
        DefaultAcsClient client = InitObject.initVodClient("LTAI4GDKuJeGsyXqwLh16gYn", "Yye0jN1m7O81zE0LHVosBz8d8ZWQer");

        GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();
        try {
            response = getVideoPlayAuth(client);
            //播放凭证
            System.out.print("PlayAuth = " + response.getPlayAuth() + "\n");
            //VideoMeta信息
            System.out.print("VideoMeta.Title = " + response.getVideoMeta().getTitle() + "\n");
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }


    /**
     * 根据视频的id获取视频的路径和基本信息
     * @throws Exception
     */
    private static void getPlayUrl() throws Exception {
        //1、根据视频id获取视频的播放地址
        //创建初始化对象
        DefaultAcsClient client = InitObject.initVodClient("LTAI4GDKuJeGsyXqwLh16gYn", "Yye0jN1m7O81zE0LHVosBz8d8ZWQer");

        //创建获取视频地址request和response
        GetPlayInfoRequest request = new GetPlayInfoRequest();

        GetPlayInfoResponse response = getPlayInfo(client);

        List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
        //播放地址
        for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
            System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
        }
        //Base信息
        System.out.print("VideoBase.Title = " + response.getVideoBase().getTitle() + "\n");

        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }

}
