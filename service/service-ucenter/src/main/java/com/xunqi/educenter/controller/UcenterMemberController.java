package com.xunqi.educenter.controller;


import com.xunqi.commonutils.JwtUtils;
import com.xunqi.commonutils.R;
import com.xunqi.educenter.entity.UcenterMember;
import com.xunqi.educenter.service.UcenterMemberService;
import com.xunqi.educenter.vo.RegisterVo;
import com.xunqi.educenter.vo.UcenterMemberVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-08
 */
@Api(value = "用户管理",tags = "用户管理")
@RestController
@RequestMapping("/educenter/member")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService memberService;

    /**
     * 用户登录
     * @param memberVo
     * @return
     */
    @ApiOperation(value = "用户登录",notes = "用户登录",httpMethod = "POST")
    @PostMapping(value = "/login")
    public R loginUser(@ApiParam(name = "memberVo",value = "登录vo对象",required = true)
                       @RequestBody UcenterMemberVo memberVo) {

        //调用service方法实现登录
        //返回token值
        String token = memberService.login(memberVo);

        return R.ok().data("token",token);
    }


    /**
     * 用户注册
     * @param registerVo
     * @return
     */
    @ApiOperation(value = "用户注册",notes = "用户注册",httpMethod = "POST")
    @PostMapping(value = "/register")
    public R register(@ApiParam(name = "registerVo",value = "注册vo对象",required = true)
            @Valid @RequestBody RegisterVo registerVo, BindingResult result) {

        if (result.hasErrors()) {
            return R.error().message("参数效验不通过");
        }

        memberService.register(registerVo);

        return R.ok();
    }


    /**
     * 根据token获取用户信息
     * @param request
     * @return
     */
    @ApiOperation(value = "根据token获取用户信息",notes = "根据token获取用户信息",httpMethod = "GET")
    @GetMapping(value = "/getMemberInfo")
    public R getMemberInfo(HttpServletRequest request) {

        //调用jwt工具类的方法，根据request对象获取请求头token信息，返回用户id
        String memberIdByJwtToken = JwtUtils.getMemberIdByJwtToken(request);

        //根据用户id查询数据库
        UcenterMember ucenterMember = this.memberService.getById(memberIdByJwtToken);

        return R.ok().data("userInfo",ucenterMember);
    }


    /**
     * 根据用户token获取用户信息,提供远程调用
     * @param jwtToken
     * @return
     */
    @GetMapping(value = "/getByTokenUcenterInfo")
    public UcenterMember getByTokenUcenterInfo(@RequestParam("jwtToken") String jwtToken) {

        UcenterMember ucenterMember = this.memberService.getById(jwtToken);

        return ucenterMember;
    }


    /**
     * 查询某一天注册人数
     * @param day
     * @return
     */
    @GetMapping(value = "/countRegister/{day}")
    public R countRegister(@PathVariable("day") String day) {

        Integer count = memberService.countRegister(day);

        return R.ok().data("countRegister",count);
    }
}

