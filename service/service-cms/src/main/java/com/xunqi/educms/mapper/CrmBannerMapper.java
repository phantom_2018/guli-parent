package com.xunqi.educms.mapper;

import com.xunqi.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-06
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
