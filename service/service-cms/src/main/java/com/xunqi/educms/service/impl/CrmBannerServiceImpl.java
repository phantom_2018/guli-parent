package com.xunqi.educms.service.impl;

import com.xunqi.educms.entity.CrmBanner;
import com.xunqi.educms.mapper.CrmBannerMapper;
import com.xunqi.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-06
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Override
    public List<CrmBanner> selectAllBanner() {

        List<CrmBanner> list = this.baseMapper.selectList(null);

        return list;
    }
}
