package com.xunqi.orderservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xunqi.commonutils.R;
import com.xunqi.orderservice.entity.Order;
import com.xunqi.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */

@RestController
@RequestMapping("/orderservice/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 生成订单
     * @param courseId
     * @return
     */
    @PostMapping(value = "/createOrder/{courseId}")
    public R createOrder(@PathVariable("courseId") String courseId, HttpServletRequest request) {

        //创建订单，返回订单号
        String orderNo = orderService.createOrderInfo(courseId,request);

        return R.ok().data("orderId",orderNo);
    }


    /**
     * 根据订单id查询订单信息
     * @param orderId
     * @return
     */
    @GetMapping(value = "/getOrderInfo/{orderId}")
    public R getOrderInfo(@PathVariable("orderId") String orderId) {

        Order orderInfo = orderService.getOne(new QueryWrapper<Order>().eq("order_no", orderId));

        return R.ok().data("orderInfo",orderInfo);
    }

    /**
     * 根据课程id和用户id查询订单表中订单的状态
     * @return
     */
    @GetMapping(value = "/isBuyCourse/{courseId}/{memberId}")
    public boolean isBuyCourse(@PathVariable("courseId") String courseId,
                               @PathVariable("memberId") String memberId) {

        boolean flag = orderService.isBuyCourse(courseId,memberId);

        return flag;
    }

}

