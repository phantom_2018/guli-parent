package com.xunqi.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-28 21:11
 **/
public interface OssService {

    String uploadFileAvatar(MultipartFile file);
}
